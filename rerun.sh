#!/bin/bash
JOBID=$(curl --header "PRIVATE-TOKEN: $1" "https://gitlab.com/api/v4/projects/$2/pipelines/$3/jobs?scope[]=success" | python3 job_id.py)
curl --request POST --header "PRIVATE-TOKEN: $1" "https://gitlab.com/api/v4/projects/$2/jobs/$JOBID/retry"
